

import es.ua.expertojava.todo.*

class BootStrap {

    def init = { servletContext ->
        try {
            def categoryHome = new Category(name:"Hogar").save()
            def categoryJob = new Category(name:"Trabajo").save()

            def tagEasy = new Tag(name:"Fácil").save()
            def tagDifficult = new Tag(name:"Difícil").save()
            def tagArt = new Tag(name:"Arte").save()
            def tagRoutine = new Tag(name:"Rutina").save()
            def tagKitchen = new Tag(name:"Cocina").save()

            def roleAdministrador = new Role(authority: "ROLE_ADMIN").save()
            def roleUsuario = new Role(authority:"ROLE_BASIC").save()

            def userAdmin = new User(username: "usuario",password: "admin",name:"haly",surnames: "hr",confirmPassword: "",email: "haly@yahoo..es" ).save()
            def userUsuario1 = new User(username: "usuario1",password: "usuario1",name:"marco",surnames: "albertsma",confirmPassword: "",email: "marco@gmail.es" ).save()
            def userUsuario2 = new User(username: "usuario2",password: "usuario2",name:"pablo",surnames: "albertsma",confirmPassword: "",email: "pablo@gmail.es" ).save()

            PersonRole.create(userAdmin,roleAdministrador)
            PersonRole.create(userUsuario1,roleUsuario)
            PersonRole.create(userUsuario2,roleUsuario)


            def todoPaintKitchen = new Todo(title:"Pintar cocina", date:new Date()+1,completed:"false",user:userUsuario1)
            def todoCollectPost = new Todo(title:"Recoger correo postal", date:new Date()+2,completed:"false",user:userUsuario1 )
            def todoBakeCake = new Todo(title:"Cocinar pastel", date:new Date()+4,completed:"false",user:userUsuario2)
            def todoWriteUnitTests = new Todo(title:"Escribir tests unitarios", date:new Date(),completed:"false",user:userAdmin)

            todoPaintKitchen.addToTags(tagDifficult)
            todoPaintKitchen.addToTags(tagArt)
            todoPaintKitchen.addToTags(tagKitchen)
            todoPaintKitchen.category = categoryHome
            todoPaintKitchen.save()

            todoCollectPost.addToTags(tagRoutine)
            todoCollectPost.category = categoryJob
            todoCollectPost.save()

            todoBakeCake.addToTags(tagEasy)
            todoBakeCake.addToTags(tagKitchen)
            todoBakeCake.category = categoryHome
            todoBakeCake.save()

            todoWriteUnitTests.addToTags(tagEasy)
            todoWriteUnitTests.category = categoryJob
            todoWriteUnitTests.save()


//            for (String url in [
//                    '/', '/index', '/index.gsp', '/**/favicon.ico',
//                    '/assets/**', '/**/js/**', '/**/css/**', '/**/images/**',
//                    '/login', '/login.*', '/login/*',
//                    '/logout', '/logout.*', '/logout/*']) {
//                new Requestmap(url: url, configAttribute: 'permitAll').save()
//            }
//            new Requestmap(url: '/profile/**',    configAttribute: 'ROLE_USER').save()
//            new Requestmap(url: '/admin/**',      configAttribute: 'ROLE_ADMIN').save()
//            new Requestmap(url: '/admin/user/**', configAttribute: 'ROLE_ADMIN').save()
//            new Requestmap(url: '/j_spring_security_switch_user',
//                    configAttribute: 'ROLE_SWITCH_USER,isFullyAuthenticated()').save()



        }catch (Exception e){
            e.printStackTrace()
        }

    }
    def destroy = { }
}


