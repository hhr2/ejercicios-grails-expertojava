package grails

class LogFilters {
    def springSecurityService
    def filters = {
        all(controller:'todo|category|tag|user', action:'*') {
            before = {

            }
            after = { Map model ->
                //                2016-02-29 23:10:33,012 [http-bio-8080-exec-9] TRACE grails.LogFilters  - User @admin - Controlador category - Accion index - Modelo [categoryInstanceCount:2, categoryInstanceList:[Hogar, Trabajo]]
                //                2015-03-28 11:13:15,863 [http-bio-8080-exec-9] TRACE todo.LogFilters  - User admin - Controlador category - Accion index - Modelo [categoryInstanceCount:2, categoryInstanceList:[Hogar, Trabajo]]
                log.trace("User ${springSecurityService.getCurrentUser()} - Controlador ${controllerName} - Accion ${actionName} - Modelo ${model}")
            }
            afterView = { Exception e ->

            }
        }
    }
}