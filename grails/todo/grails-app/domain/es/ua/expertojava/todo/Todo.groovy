

package es.ua.expertojava.todo

class Todo {
    String title
    Boolean completed
    String description
    Date date
    Date reminderDate
    String url
    Category category
    User user
    //Auditoria
    Date dateCreated
    Date lastUpdated
    //
    Date dateDone

    static hasMany = [tags:Tag]
    static belongsTo = [Tag,User]




    static constraints = {
        title(blank:false)
        completed(blank: false)
        description(blank:true, nullable:true, maxSize:1000)
        date(nullable:false)
        reminderDate(nullable:true)
        url(nullable:true, url:true)
        category(nullable:true)
        user(nullable: true)

        reminderDate(nullable:true,
                    validator: { val, obj ->
                                    if (val && obj.date) {
                                        return val.before(obj?.date)
                                    }
                                    return true }
    )
        dateDone(nullable:true)
}
    String toString(){
        "title"
    }
}

