package es.ua.expertojava.todo



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)

class TodoController {

    def todoService

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond Todo.list(params), model:[todoInstanceCount: Todo.count()]
    }

    def show(Todo todoInstance) {
        respond todoInstance
    }

    def create() {
        respond new Todo(params)
    }


    def showUserTodos(){
        respond todoService.showUserTodos2(), view:'index'
    }

    def mostrarTareasByCategorias(){
        ArrayList categoriasTareas = params.list('categoriaID')
        respond todoService.showUserTodosByCategorias(categoriasTareas), view:'index'
    }

    def listByCategory(){
        def listTodosUser = todoService.showUserTodos2()
        def listaCategorias = new ArrayList()

        listTodosUser.each{todo ->
            if(todo.getCategory() != null){
                if(listaCategorias.find{it.name==todo.category.name} == null){
                    listaCategorias.add(todo.category)
                }
            }
        }
        respond listaCategorias, view:'listByCategory'
    }

    @Transactional
    def save(Todo todoInstance) {

        if (todoInstance == null) {
            notFound()
            return
        }

        if (todoInstance.hasErrors()) {
            respond todoInstance.errors, view:'create';
            return
        }

        todoService.saveTodo(todoInstance)


        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'todo.label', default: 'Todo'), todoInstance.title])
                redirect todoInstance
            }
            '*' { respond todoInstance, [status: CREATED] }
        }
    }


    def edit(Todo todoInstance) {
        respond todoInstance
    }

    @Transactional
    def update(Todo todoInstance) {
        if (todoInstance == null) {
            notFound()
            return
        }

        if (todoInstance.hasErrors()) {
            respond todoInstance.errors, view:'edit'
            return
        }

        todoService.saveTodo(todoInstance)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'Todo.label', default: 'Todo'), todoInstance.title])
                redirect todoInstance
            }
            '*'{ respond todoInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(Todo todoInstance) {

        if (todoInstance == null) {
            notFound()
            return
        }
        else {
            todoService.delete(todoInstance)

        }

//        todoInstance.delete flush:true
//
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Todo.label', default: 'Todo'), todoInstance.title])
                redirect action:"showUserTodos", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'todo.label', default: 'Todo'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }

    def lastTodosDoneRequest(){

        respond todoService.lastTodosDone(-5),view:'index'
    }
}

