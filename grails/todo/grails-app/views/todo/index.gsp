
<%@ page import="es.ua.expertojava.todo.Todo" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'todo.label', default: 'Todo')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-todo" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
				<li><g:link class="home" controller="todo" action="listByCategory"><g:message code="default.report.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-todo" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>
					
						<g:sortableColumn property="title" title="${message(code: 'todo.title.label', default: 'Title')}" />
					
						<g:sortableColumn property="completed" title="${message(code: 'todo.completed.label', default: 'Completed')}" />
					
						%{--<g:sortableColumn property="description" title="${message(code: 'todo.description.label', default: 'Description')}" />--}%
					
						<g:sortableColumn property="date" title="${message(code: 'todo.date.label', default: 'Date')}" />
					
						%{--<g:sortableColumn property="reminderDate" title="${message(code: 'todo.reminderDate.label', default: 'Reminder Date')}" />--}%
					
						<g:sortableColumn property="url" title="${message(code: 'todo.url.label', default: 'Url')}" />


					</tr>
				</thead>
				<tbody>
				<g:each in="${todoInstanceList}" status="i" var="todoInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${todoInstance.id}">${fieldValue(bean: todoInstance, field: "title")}</g:link></td>
					
						%{--4.8.2 haly etiqueta creada en TodoTagLibrary--}%
						<td id="td_img" class="img_comp"><todo:printIconFromBoolean value="${todoInstance.completed}"/></td>


						%{--4.8.1 haly --<td>${fieldValue(bean: todoInstance, field: "description")}</td>--}%

						<td><g:formatDate date="${todoInstance.date}" /></td>

						%{--4.8.1haly --<td><g:formatDate date="${todoInstance.reminderDate}" /></td>--}%

%{-- 4.8.1 haly --*******Se agregan a la lista de tareas las opciones de Eliminar o Editar--}%
						<td>${fieldValue(bean: todoInstance, field: "url")}</td>
						<td><g:form url="[resource:todoInstance, action:'delete']" method="DELETE">
							<fieldset class="buttons">
								<g:link class="edit" action="edit" resource="${todoInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
								<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
							</fieldset>
						</g:form></td>

					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${todoInstanceCount ?: 0}" />
			</div>
		</div>
	</body>
</html>
