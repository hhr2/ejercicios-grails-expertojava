<%@ page import="es.ua.expertojava.todo.Todo" %>

<div class="nav" role="navigation">
    <ul>
        <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
        <li><g:link class="list" action="showUserTodos"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
        <li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
        <li><g:link class="report" action="listByCategory"><g:message code="default.report.label" args="[entityName]" /></g:link></li>
        <li><g:link class="report" action="lastTodosDoneRequest"><g:message code="default.lasttodosdone.label" args="[entityName]" /></g:link></li>
    </ul>
</div>
