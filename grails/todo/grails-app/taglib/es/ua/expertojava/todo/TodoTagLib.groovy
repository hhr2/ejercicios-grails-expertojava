package es.ua.expertojava.todo

class TodoTagLib {
    //static defaultEncodeAs = [taglib:'html']
    //static encodeAsForTags = [tagName: [taglib:'html'], otherTagName: [taglib:'none']]
    static namespace = 'todo'


//********Ejercicio 4.8.2
    def printIconFromBoolean = { attrs ->
        def value = attrs ['value']
        if(value) {

            out <<asset.image(src:"iconOK.png")


        }else{
            out <<asset.image(src:"iconKO.png")
        }
    }


}
