package es.ua.expertojava.todo

import grails.test.mixin.TestFor
import spock.lang.Specification
import spock.lang.Unroll

/**
 * See the API for {@link grails.test.mixin.domain.DomainClassUnitTestMixin} for usage instructions
 */
@TestFor(Category)
class CategorySpec extends Specification {

    def setup() {
    }

    def cleanup() {
    }

    def "El nombre de la categoría no puede ser la cadena vacía"() {
        given:
        def c1 = new Category(name:"")
        when:
        c1.validate()
        then:
        c1?.errors['name']
    }

    def "Si el nombre no es la cadena vacía, este campo no dará problemas"() {
        given:
        def c1 = new Category(name:"algo")
        when:
        c1.validate()
        then:
        !c1?.errors['name']
    }

    def "La descripción de la categoría puede ser la cadena vacía"() {
        given:
        def c1 = new Category(description: "")
        when:
        c1.validate()
        then:
        !c1?.errors['description']
    }

    def "Si la descripción es la cadena vacía, este campo no dará problemas"() {
        given:
        def c1 = new Category(description:"")
        when:
        c1.validate()
        then:
        !c1?.errors['description']
    }

    def "La descripción de la categoría puede ser null"() {
        given:
        def c1 = new Category(description: null)
        when:
        c1.validate()
        then:
        !c1?.errors['description']
    }

    @Unroll
    def "Si la descripción tiene menos de 1001 caracteres, no dará problemas"() {
        given:
        def c1 = new Category(description: "a"*characters)
        when:
        c1.validate()
        then:
        !c1?.errors['description']
        where:
        characters << [0,1,999,1000]
    }

    @Unroll
    def "Si la descripción tiene más 1000 caracteres, dará problemas"() {
        given:
        def c1 = new Category(description: "a"*characters)
        when:
        c1.validate()
        then:
        c1?.errors['description']
        where:
        characters << [1001,1002]
    }
    def "La instancia de Categoría devuelve su nombre por defecto"() {
        expect:
        new Category(name:"The category name").toString() == "The category name"
    }
}

