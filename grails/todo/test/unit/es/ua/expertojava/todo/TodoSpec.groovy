package es.ua.expertojava.todo

import grails.test.mixin.TestFor
import spock.lang.Specification
import spock.lang.Unroll

/**
 * See the API for {@link grails.test.mixin.domain.DomainClassUnitTestMixin} for usage instructions
 */
@TestFor(Todo)
class TodoSpec extends Specification {

    def setup() {
    }

    def cleanup() {
    }

    def "El titulo de la tarea no puede ser la cadena vacía"() {
        given:
        def t1 = new Todo(title: "")
        when:
        t1.validate()
        then:
        t1?.errors['title']
    }
//
//    def "Si el titulo no es la cadena vacía, este campo no dará problemas"() {
//        given:
//        def t1 = new Todo(titulo:"algo")
//        when:
//        t1.validate()
//        then:
//        !t1?.errors['title']
//    }



    @Unroll
    def " Si la reminderdate #reminderDate es posterior a la date #date, no dara problemas"() {
        given:
        def t1 = new Todo()
        t1.date= date
        t1.reminderDate=reminderDate
        when:
        t1.validate()
        then:
        !t1?.errors['reminderDate']
        where:
        date             |   reminderDate
        new Date()       |   new Date()-1

    }
    @Unroll
    def " la reminderdate #reminderDate no puede ser posterior a la date #date"() {
        given:
        def t1 = new Todo()
        t1.date= date
        t1.reminderDate=reminderDate
        when:
        t1.validate()
        then:
        t1?.errors['reminderDate']
        where:
        date             |   reminderDate
        new Date()       |   new Date()
        new Date()       |   new Date() + 1

    }




}
