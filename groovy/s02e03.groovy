/* Añade aquí la implementación solicitada en el ejercicio */

def localeEN = "en_EN"
def localeUS = "en_US"
def localeES = "es_ES"

def EN = "£"
def US = "\$"
def ES = "€"

Number.metaClass.moneda = { locale ->
    switch(locale){
        case localeEN: "$EN$delegate" ; break;
        case localeUS: "$US$delegate" ; break;
        case localeES: "$delegate$ES" ; break;
    }  

}

assert 10.2.moneda("en_EN") ==  "£10.2"
assert 10.2.moneda("en_US") ==  "\$10.2"
assert 10.2.moneda("es_ES") ==  "10.2€"

assert 10.moneda("en_EN") ==  "£10"
assert 10.moneda("en_US") ==  "\$10"
assert 10.moneda("es_ES") ==  "10€"

assert new Float(10.2).moneda("en_EN") ==  "£10.2"
assert new Float(10.2).moneda("en_US") ==  "\$10.2"
assert new Float(10.2).moneda("es_ES") ==  "10.2€"

assert new Double(10.2).moneda("en_EN") ==  "£10.2"
assert new Double(10.2).moneda("en_US") ==  "\$10.2"
assert new Double(10.2).moneda("es_ES") ==  "10.2€"